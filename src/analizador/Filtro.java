/*
 * Filtro.java
 *
 * Created on December 4, 2006, 11:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package analizador;
import java.io.File;
import javax.swing.filechooser.FileFilter;
/**
 *
 * @author alumno
 */
public class Filtro extends FileFilter{
    
    /** Creates a new instance of Filtro */
    public Filtro() {
    }
    public boolean accept(File f){
        if(f.isDirectory())
            return true;
        String s=f.getName();
        int i=s.lastIndexOf('.');
        if(i>0&&i<s.length()-1){
            String extencion=s.substring(i+1).toLowerCase();
            if(extencion.compareTo("vak")==0){
                return true;
            }
        }
        return false;
    }
    public String getDescription(){
        return "archivos (*.vak)";
    }
}
