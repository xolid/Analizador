/*
 * Analisis.java
 *
 * Created on December 4, 2006, 12:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package analizador;
import java.util.*;
/**
 *
 * @author alumno
 */
public class Analisis {
    String linea;
    int num;
    public String codigoJava;
    Variables var=new Variables();
    /** Creates a new instance of Analisis */
    public Analisis() {
        linea="";
        num=0;
        codigoJava="import java.io.*; \n" +
                   "public class Clase{ \n";
    }
    public void setLinea(String linea) {
        this.linea=linea;
    }
    public String evaluar(int numlin,int linfin) {
        num++;
        linea=quitarEspaciosIniciales(linea);
        String car=validarLinea(linea);
        if(car!="")
            return "Se encontro un caracter invalido '"+car+"'.";
        if(num==1||linea.compareTo("inicio")==0){
            if(num>1||linea.compareTo("inicio")!=0)
                return "No ha definido correctamente un inicio de codigo.";
            codigoJava+="public static void main(String []args){\n"+
                        "InputStreamReader isr=new InputStreamReader(System.in); \n" +
                        "BufferedReader br=new BufferedReader(isr); \n";
        }
        else
            if(linea.compareTo("fin")==0){
                if(numlin!=linfin)
                    return "Aun no puede cerrar el segmento de codigo.";
                codigoJava+="}\n"+
                            "}";
            }
            else{
                String tipo;
                String expresion;
                String espacio;
                try{
                    tipo=linea.substring(0,3);
                    expresion=linea.substring(3,linea.length());
                    espacio=linea.substring(3,4);
                }
                catch(Exception e){
                    return "Formato de expresion desconocido.";
                }
                if(espacio.compareTo(" ")!=0)
                    return "No contiene un espacio para dividir el operador de los operandos.";
                if(tipo.compareTo("int")==0){
                    expresion=quitarEspaciosIniciales(expresion);
                    if(buscarCaracteres(expresion,",")){
                        if(!buscarCaracteres(expresion,";"))
                            return "No existe un caracter de termino \";\" para la expresion.";
                        String var1=expresion.substring(0,expresion.indexOf(","));
                        String var2=expresion.substring(1+expresion.indexOf(","),expresion.indexOf(";"));
                        if(buscarCaracteres(var2,","))
                            return "Maximo dos declaraciones por \"int\"";
                        if(var1.compareTo("")==0||var2.compareTo("")==0){
                            return "Requiere definir variables aqui.";
                        }
                        var.agregar_var(var1);
                        var.agregar_var(var2);
                        codigoJava+="int "+var1+","+var2+";\n";
                    }
                    else{
                        if(!buscarCaracteres(expresion,";"))
                            return "No existe un caracter de termino \";\" para la expresion.";
                        String var1=expresion.substring(0,expresion.indexOf(";"));
                        if(var1.compareTo("")==0)
                            return "Requiere definir una variable aqui.";
                        var.agregar_var(var1);
                        codigoJava+="int "+var1+";\n";
                    }
                }
                else
                    if(tipo.compareTo("cha")==0){
                    expresion=quitarEspaciosIniciales(expresion);
                    if(buscarCaracteres(expresion,",")){
                        if(!buscarCaracteres(expresion,";"))
                            return "No existe un caracter de termino \";\" para la expresion.";
                        String var1=expresion.substring(0,expresion.indexOf(","));
                        String var2=expresion.substring(1+expresion.indexOf(","),expresion.indexOf(";"));
                        if(buscarCaracteres(var2,","))
                            return "Maximo dos declaraciones por \"int\"";
                        if(var1.compareTo("")==0||var2.compareTo("")==0){
                            return "Requiere definir variables aqui.";
                        }
                        var.agregar_var(var1);
                        var.agregar_var(var2);
                        codigoJava+="char "+var1+","+var2+";\n";
                    }
                    else{
                        if(!buscarCaracteres(expresion,";"))
                            return "No existe un caracter de termino \";\" para la expresion.";
                        String var1=expresion.substring(0,expresion.indexOf(";"));
                        if(var1.compareTo("")==0)
                            return "Requiere definir una variable aqui.";
                        var.agregar_var(var1);
                        codigoJava+="char "+var1+";\n";
                    }
                }
                else
                    if(tipo.compareTo("add")==0){
                        expresion=quitarEspaciosIniciales(expresion);
                        String errores=identificarErrores(expresion,1);
                        return errores;
                    }
                    else
                        if(tipo.compareTo("sub")==0){
                            expresion=quitarEspaciosIniciales(expresion);
                            String errores=identificarErrores(expresion,2);
                            return errores;
                        }
                        else
                            if(tipo.compareTo("div")==0){
                                expresion=quitarEspaciosIniciales(expresion);
                                String errores=identificarErrores(expresion,3);
                                return errores;
                            }
                            else
                                if(tipo.compareTo("mul")==0){
                                    expresion=quitarEspaciosIniciales(expresion);
                                    String errores=identificarErrores(expresion,4);
                                    return errores;
                                }
                                else
                                    if(tipo.compareTo("dis")==0){
                                        expresion=quitarEspaciosIniciales(expresion);
                                        String errores=identificarFunciones(expresion,1,1);
                                        return errores;
                                    }
                                    else
                                        if(tipo.compareTo("get")==0){
                                            expresion=quitarEspaciosIniciales(expresion);
                                            String errores=identificarFunciones(expresion,2,2);
                                            return errores;
                                        }
                                        else
                                            if(tipo.compareTo("imp")==0){
                                                expresion=quitarEspaciosIniciales(expresion);
                                                String errores=identificarFunciones(expresion,2,3);
                                                return errores;
                                            }
                                            else
                                                if(tipo.compareTo("inc")==0){
                                                    expresion=quitarEspaciosIniciales(expresion);
                                                    String errores=identificarFunciones(expresion,2,4);
                                                    return errores;
                                                }
                                                else
                                                    if(tipo.compareTo("dec")==0){
                                                        expresion=quitarEspaciosIniciales(expresion);
                                                        String errores=identificarFunciones(expresion,2,5);
                                                        return errores;
                                                    }
                                                    else
                                                        if(tipo.compareTo("sqr")==0){
                                                            expresion=quitarEspaciosIniciales(expresion);
                                                            String errores=identificarErrores(expresion,5);
                                                            return errores;
                                                        }
                                                        else
                                                            return "Operacion \""+linea+"\" desconocida en el lenguaje.";
            }
        return "";
    }
    private String identificarErrores(String expresion,int operacion){
        if(!buscarCaracteres(expresion,","))
            return "No existe un separador \",\" para la expresion.";
        if(!buscarCaracteres(expresion,";"))
            return "No existe un caracter de termino \";\" para la expresion.";
        String var1=expresion.substring(0,expresion.indexOf(","));
        String var2=expresion.substring(1+expresion.indexOf(","),expresion.indexOf(";"));
        if(var.buscar_var(var1)==false)
            return "Desconocida primer variable "+var1;
        if(var.buscar_var(var2)==false)
            return "Desconocida segunda variable "+var2;
        if(operacion==1)
            codigoJava+=var1+"="+var1+"+"+var2+";\n";
        if(operacion==2)
            codigoJava+=var1+"="+var1+"-"+var2+";\n";
        if(operacion==3)
            codigoJava+=var1+"="+var1+"/"+var2+";\n";
        if(operacion==4)
            codigoJava+=var1+"="+var1+"*"+var2+";\n";
        if(operacion==5)
            codigoJava+=var1+"=(int)Math.sqrt((double)"+var2+");\n";
        return "";
    }
    private String identificarFunciones(String expresion,int tipo,int operacion){
        String str1="";
        String var1="";
        if (tipo==1){
            if(!buscarCaracteres(expresion,";"))
                return "No existe un caracter de termino \";\" para la expresion.";
            str1=expresion.substring(0,expresion.indexOf(";"));
            if(!(str1.indexOf("\"")==0&&str1.lastIndexOf("\"")==str1.length()-1)){
                return "Uso invalido o nulo de las \" ";
            }
        }
        else{
            if(!buscarCaracteres(expresion,";"))
                return "No existe un caracter de termino \";\" para la expresion.";
            var1=expresion.substring(0,expresion.indexOf(";"));
            if(var.buscar_var(var1)==false)
                return "Desconocida la variable "+var1;
        }
        if(operacion==1){
            codigoJava+="System.out.printl("+str1+");\n";
        }
        if(operacion==2){
            
        }
        if(operacion==3){
            codigoJava+="System.out.printl(\"\"+"+var1+");\n";
        }
        if(operacion==4){
            codigoJava+=var1+"++;\n";
        }
        if(operacion==5){
            codigoJava+=var1+"--;\n";
        }
        return "";
    }
    private String quitarEspaciosIniciales(String cadena){
        Vector v=new Vector();
        String nuevaLinea="";
        for(int i=0;i<cadena.length();i++){
            v.addElement(""+cadena.charAt(i));
        }
        while(v.firstElement().toString().compareTo(" ")==0){
            v.remove(" ");
        }
        for(int i=0;i<v.size();i++){
            nuevaLinea+=""+v.elementAt(i);
        }
        return nuevaLinea;
    }
    public boolean buscarCaracteres(String cadena,String caracter){
        if(cadena.indexOf(caracter)==-1)
            return false;
        else
            return true;
    }
    public String validarLinea(String cadena){
        int i=0;
        for(;i<cadena.length();i++){
            if(cadena.charAt(i)=='='||cadena.charAt(i)=='"'||cadena.charAt(i)==' '||cadena.charAt(i)==','||cadena.charAt(i)==';'||cadena.charAt(i)>47&&cadena.charAt(i)<58||cadena.charAt(i)>63&&cadena.charAt(i)<91||cadena.charAt(i)>96&&cadena.charAt(i)<123){
            }
            else{
                return ""+cadena.charAt(i);
            }
        }
        return "";
    }
}
//cadena.charAt(i)<65||cadena.charAt(i)>90&&