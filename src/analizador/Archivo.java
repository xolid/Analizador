/*
 * Archivo.java
 *
 * Created on December 4, 2006, 11:39 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package analizador;
import java.io.*;
import javax.swing.*;
/**
 *
 * @author alumno
 */
public class Archivo {
    RandomAccessFile archivo;
    /** Creates a new instance of Archivo */
    public Archivo(String nombre) {
        try{
            this.archivo=new RandomAccessFile(nombre,"rw");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(new JFrame(),"Error en al abrir el archivo...","Advertencia!!!",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public void cerrar(){
        try{
            this.archivo.close();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(new JFrame(),"Error en el cierre del archivo...","Advertencia!!!",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public String leer(){
        String cadena="";
        try{
            cadena=archivo.readLine();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(new JFrame(),"Error de lectura del archivo...","Advertencia!!!",JOptionPane.INFORMATION_MESSAGE);
        }
        return cadena;
    }
    public void guardar(String cadena){
        try{
            archivo.writeBytes(cadena);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(new JFrame(),"Error de escritura del archivo...","Advertencia!!!",JOptionPane.INFORMATION_MESSAGE);
        }
    }
}